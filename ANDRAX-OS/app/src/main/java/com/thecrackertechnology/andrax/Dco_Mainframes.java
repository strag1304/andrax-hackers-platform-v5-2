package com.thecrackertechnology.andrax;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;

import com.thecrackertechnology.dragonterminal.bridge.Bridge;

public class Dco_Mainframes extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);


        setContentView(R.layout.dco_mainframes);

        CardView cardviewtpxbrute = findViewById(R.id.card_view_tpxbrute);
        CardView cardviewcicspwn = findViewById(R.id.card_view_cicspwn);
        CardView cardviewcicsshot = findViewById(R.id.card_view_cicsshot);
        CardView cardviewnetEBCDICat = findViewById(R.id.card_view_netEBCDICat);
        CardView cardviewTShOcker = findViewById(R.id.card_view_TShOcker);
        CardView cardviewphatso = findViewById(R.id.card_view_phatso);
        CardView cardviewmfsniffer = findViewById(R.id.card_view_mfsniffer);
        CardView cardviewpsikotik = findViewById(R.id.card_view_psikotik);
        CardView cardviewbirp = findViewById(R.id.card_view_birp);
        CardView cardviewmaintp = findViewById(R.id.card_view_maintp);
        CardView cardviewmainframe_bruter = findViewById(R.id.card_view_mainframe_bruter);
        CardView cardviewmfdos = findViewById(R.id.card_view_mfdos);


        cardviewtpxbrute.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("TPX_Brute -h");

            }
        });

        cardviewcicspwn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("cicspwn -h");

            }
        });

        cardviewcicsshot.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("cicsshot -h");

            }
        });

        cardviewnetEBCDICat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("netEBCDICat -h");

            }
        });

        cardviewTShOcker.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("TShOcker -h");

            }
        });

        cardviewphatso.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("phatso -h");

            }
        });

        cardviewmfsniffer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("MFSniffer -h");

            }
        });

        cardviewpsikotik.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("psikotik -h");

            }
        });

        cardviewbirp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("birp -h");

            }
        });

        cardviewmaintp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("MainTP -h");

            }
        });

        cardviewmainframe_bruter.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("mainframe_bruter -h");

            }
        });

        cardviewmfdos.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("MFDoS -h");

            }
        });

    }

    public void run_hack_cmd(String cmd) {

        Intent intent = Bridge.createExecuteIntent(cmd);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);

    }

    @Override
    public void onPause() {

        super.onPause();
        finish();
    }

}
